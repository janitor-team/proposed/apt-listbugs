
Frequently Asked Questions on **apt-listbugs**
==============================================


How can I (temporarily) disable `apt-listbugs`?
-----------------------------------------------

Sometimes you may need to disable the invocation of `apt-listbugs`, so
that you can run APT, or another similar package manager, without
also running `apt-listbugs`. Hopefully, this will happen very infrequently
and you will immediately want to re-enable `apt-listbugs`...  ;-)

In order to temporarily disable the invocation of `apt-listbugs` by APT,
there's no need to remove the package: it is enough to edit
`/etc/apt/apt.conf.d/10apt-listbugs` and turn the following line

    DPkg::Pre-Install-Pkgs {"/usr/bin/apt-listbugs apt";};

into a comment (by prepending the "//" comment symbol).

Or alternatively, you may just move the file out of the way:

    # mv /etc/apt/apt.conf.d/10apt-listbugs /root/

When you want to re-enable the invocation of `apt-listbugs`, drop the
comment symbol from the line, or move the file back to its original
location:

    # mv /root/10apt-listbugs /etc/apt/apt.conf.d/


How can I use `apt-listbugs` with **apt-cacher**/**apt-cacher-ng** proxies?
---------------------------------------------------------------------------

When installing **apt-cacher** or **apt-cacher-ng** with proxy configuration,
you will have configured APT proxy settings to use `apt-cacher`.
`apt-listbugs` will also use that configuration, and since `apt-cacher`
won't handle POST requests directed to the BTS, it will fail.

You will have a configuration like:

    Acquire::http { Proxy "http://CacheServerIp:3142"; };

for `apt-cacher`; add a specific proxy configuration line for
bugs.debian.org so that `apt-listbugs` can use a different
configuration:

    Acquire::http::Proxy::bugs.debian.org "DIRECT";


How can I use `apt-listbugs` in unattended installations/upgrades?
------------------------------------------------------------------

`apt-listbugs` requires a console for user interaction. This could
make coexistence with unattended package installations/upgrades
(e.g.: see the **cron-apt** package) a little harder.

With its default configuration, `apt-listbugs` will stop any
automatic installation or upgrade, whenever the packages to be
installed or upgraded are affected by RC bugs. Buggy packages
will also be automatically pinned.

If you want the unattended process to upgrade the remaining packages, after
`apt-listbugs` has pinned all the buggy packages, you will have to
configure the unattended process to attempt the upgrade twice in a row:
the first attempt will pin all the buggy packages, if any; the second
attempt will upgrade the remaining packages.

On the other hand, if you want `apt-listbugs` to do nothing at all
during unattended package installations/upgrades, you may configure the
unattended process to set the `APT_LISTBUGS_FRONTEND` environment
variable to "none" before invoking the package manager.


Why does `apt-listbugs` ignore the "affects" field?
---------------------------------------------------

Sometimes a bug has an effect on package **A**, but is actually due to
a flaw in package **B**. The BTS is able to track this kind of situation:
the bug report should be assigned to package **B** (where the bug can
actually be fixed), but marked as affecting package **A**, by using the
"affects" BTS command.
Nonetheless, `apt-listbugs` currently ignores the "affects" field.
In other words, when examining the bugs of package **A**, `apt-listbugs`
does not take bugs affecting **A** into account; it only looks at the bugs
actually assigned to **A**.
This is intentional, for the reasons explained below.

When a bug is assigned to package **B**, and found in version b1, but
marked as affecting package **A**, this may correspond to one of two
possible scenarios:

 0. the bug is actually present in **B**/b1, but causes breakage in
    package **A**; hence, do not upgrade to **B**/b1 if you want to avoid
    breaking package **A**

 1. the bug is actually present in **B**/b1, but only shows up when a
    given version (say, a1) of package **A** is installed; hence, do not
    upgrade to **A**/a1 if you want to prevent the bug in package **B** from
    showing up

In scenario 0, we should not stop an upgrade (or an installation) of
package **A** because of the bug that affects **A**: package **B** is
the only one which is able to introduce the bug into a system. Hence,
as long as the bug is correctly assigned to **B** and marked as found in
version b1, `apt-listbugs` will give users the opportunity to stop an upgrade
to **B**/b1, but, on the other hand, will ignore the bug when upgrading
package **A**.
This is obtained by ignoring the "affects" field, which is exactly
what `apt-listbugs` does.

In scenario 1, stopping an upgrade to **A**/a1 would actually be useful,
as it would prevent the bug from showing up. However, the "affects"
field does not carry any version tracking information, unfortunately.
This means that there's no way for `apt-listbugs` to know that the
problematic upgrade is the one to **A**/a1, rather than to **A**/a2 or to
**A**/a3. And there's no way for `apt-listbugs` to distinguish between
scenario 0 and scenario 1, either. Hence, `apt-listbugs` currently
ignores the "affects" field in any case.

There's a way to work around the lack of version tracking info
for the "affects" field: whenever you find yourself in scenario 1,
file a separate bug report against package **A**, marking it as found in
version a1 and as blocked by the actual bug report (the one assigned
to package **B**). Please choose a descriptive bug report title, citing
the interaction with package **B**, in order to let users quickly see
whether they should worry about the issue.
The bug report against package **A** could request the addition of an
appropriate "Breaks: B (<< b2)" field to the control file.
Please note that b2 is the version where the actual bug in package **B**
is fixed, hence the bug report against **A** cannot be closed until the
bug in **B** is fixed: this is correctly expressed by the block relationship
between the two bug reports.
Anyway, please take into account that this strategy is sub-optimal
and may annoy some package maintainers!

Everything said so far holds for the `apt-listbugs` "apt" command.
The other `apt-listbugs` commands ("list" and "rss") need to behave
consistently, or otherwise they will produce misleading output.

Please note that scenario 1 could be dealt with differently, if
the BTS added version tracking info for the "affects" field,
or, maybe, if it implemented a distinct way to express scenario 1,
so that it's not confused with scenario 0 and has version tracking
info. See
[these](http://lists.debian.org/debian-devel/2011/10/msg00446.html) two
[messages](http://lists.debian.org/debian-devel/2011/10/msg00502.html)
for more details.
If there's consensus on this idea, a wishlist bug report should
probably be filed against the **debbugs** package, requesting this
new feature.


How are back-ports handled by `apt-listbugs`?
---------------------------------------------

When determining whether a given version of a package includes a given
bug, `apt-listbugs` treats a back-ported version (such as x.y-z~bpo8-r)
as if it were the corresponding official Debian package version (x.y-z).
This special handling is based on the (hopefully reasonable) assumption
that a back-port has the same bugs as the corresponding official package.


How can I debug `apt-listbugs` and file bug reports against it?
---------------------------------------------------------------

When there is an error, to obtain useful debugging information,
please run `apt-listbugs` with the debug option `-d`.

If the error was encountered when using `apt-listbugs list` or
`apt-listbugs rss`, just re-run the command-line with the `-d` option
added, and attach the output along with your bug report.

If the error was encountered when using `apt-listbugs apt` (within
an APT session), you may enable debug output by editing
`/etc/apt/apt.conf.d/10apt-listbugs` to add the debug option `-d`:
the following line

    DPkg::Pre-Install-Pkgs {"/usr/bin/apt-listbugs apt";};

should become

    DPkg::Pre-Install-Pkgs {"/usr/bin/apt-listbugs apt -d";};

Then please restart the APT session, and attach the output along with
your bug report.


Hacking `apt-listbugs`: where can I find information on the BTS SOAP interface?
-------------------------------------------------------------------------------

There is so much more information about the BTS SOAP interface
available from debbugs
[source](http://bugs.debian.org/debbugs-source/mainline/Debbugs/Bugs.pm)

